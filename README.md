## Important Note

We are migrating to github.io. Visit our new version [Here](https://lightbulb128.github.io/touhou-card-player/). This place is archived.

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## For development

```bash
npm run dev
```

## For deployment
```bash
npm run build
npm run start
```

## Resources

This project is initially written for playing Touhou music-character card games. The resources are posted on [Google Drive](https://drive.google.com/drive/folders/1KH6aC5oPLx7pel1fZ1UHxudIUdJSvhru?usp=sharing).